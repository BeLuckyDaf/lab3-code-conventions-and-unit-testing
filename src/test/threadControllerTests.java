import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.controllers.threadController;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class threadControllerTests {
    private Thread stub;
    private String slug = "some";
    private String id = "1";

    @BeforeEach
    @DisplayName("thread test")
    void createThreadTest() {
        stub = new Thread("thread", new Timestamp(0), "forum", "message", slug, "hello world", 42);
    }

    @Test
    @DisplayName("Check id or slug")
    void checkIdOrSlugTest() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt(id))).thenReturn(stub);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(stub);
            // Existing
            assertEquals(stub, controller.CheckIdOrSlug(id), "Received thread by ID.");
            assertEquals(stub, controller.CheckIdOrSlug(slug), "Received thread by slug.");

            // Non-existing
            assertNull(controller.CheckIdOrSlug("wrongSlug"));
            assertNull(controller.CheckIdOrSlug("2")); //wrong id
        }
    }

    @Test
    @DisplayName("Create posts")
    void createPostTest() {
        List<Post> postsStub = Collections.emptyList();
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(stub);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postsStub), controller.createPost(slug, postsStub));
        }
    }

    @Test
    @DisplayName("Get posts")
    void postsTest() {
        List<Post> postsStub = Collections.emptyList();
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getPosts(stub.getId(), 100, 1, null, false)).thenReturn(postsStub);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(stub);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(postsStub), controller.Posts(slug, 100, 1, null, false));
        }
    }

    @Test
    @DisplayName("Change thread")
    void changeTest() {
        Thread changeThread = new Thread("super person", new Timestamp(2), "forum", "super message", "changeSlug", "tooopppic", 4142);
        changeThread.setId(1);
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("changeSlug")).thenReturn(changeThread);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(stub);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(stub), controller.change("changeSlug", stub), "Thread changed.");
        }
    }

    @Test
    @DisplayName("Info thread")
    void infoTest() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(stub);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(stub), controller.info(slug), "Got thread info.");
        }
    }

    @Test
    @DisplayName("Create vote")
    void createVoteTest() {
        Vote vote = new Vote("super person", 4);
        User user = new User("super person", "d.medvedev@gov.ru", "Vladimir Deuster", "Top Dog");
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try(MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(stub);
                userMock.when(() -> UserDAO.Info("super person")).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(stub), controller.createVote(slug, vote), "Created vote.");
            }
        }
    }
}

